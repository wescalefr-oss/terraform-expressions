locals {
    sg_list = {
        sg1 = {
          cidr_blocks  = ["0.0.0.0/0"]
          description = "Security group"
          from_port = 443
          to_port = 443
          protocol = "tcp"
          tags = ["dev", "prod", "preprod", "sandbox"]
        }
        sg2 = {
          cidr_blocks  = ["0.0.0.0/0"]
          description = "Security group"
          from_port = 80
          to_port = 80
          protocol = "tcp"
          tags = ["prod", "preprod"]
        }
        sg3 = {
          cidr_blocks  = ["0.0.0.0/0"]
          description = "Security group"
          from_port = 22
          to_port = 22
          protocol = "tcp"
          tags = ["dev", "sandbox"]
        }
        sg4 = {
          cidr_blocks  = ["0.0.0.0/0"]
          description = "Security group"
          from_port = 9090
          to_port = 9090
          protocol = "tcp"
          tags = ["dev", "prod", "preprod", "sandbox"]
        }
        sg5 = {
          cidr_blocks  = ["0.0.0.0/0"]
          description = "Security group"
          from_port = 9123
          to_port = 9123
          protocol = "tcp"
          tags = ["dev", "prod", "preprod", "sandbox"]
        }
        sg6 = {
          cidr_blocks  = ["0.0.0.0/0"]
          description = "Security group"
          from_port = 5678
          to_port = 5676
          protocol = "tcp"
          tags = ["dev"] 
        }
        sg7 = {
          cidr_blocks  = ["0.0.0.0/0"]
          description = "Security group"
          from_port = 8080
          to_port = 8080
          protocol = "tcp"
          tags = ["sandbox"]
        }
    }
}
