# terraform-expressions

Exercice 3 : 

Le but de cet exercice est de créer via une seule ressource aws_security_group plusieurs security groups, il faudra créer chaque security group avec un seul tag.
et chaque security group devra porter le nom du sg + le tag
Si vous avez réussi, vous devriez avoir 18 security groups au total.


les commandes pour tester votre code terraform : 

$ export AWS_ACCESS_KEY_ID="anaccesskey"  
$ export AWS_SECRET_ACCESS_KEY="asecretkey"  
$ terraform init  
$ terraform plan  
