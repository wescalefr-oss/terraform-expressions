# terraform-expressions

Exercice 4 :

Le but de cet exercice est de créer les security groups et leurs rules à partir des listes et maps disponibles en variable locale. Idéalement via deux ressources boucles.

ATTENTION, cet exercice est le plus compliqué :)


les commandes pour tester votre code terraform : 

$ export AWS_ACCESS_KEY_ID="anaccesskey"  
$ export AWS_SECRET_ACCESS_KEY="asecretkey"  
$ terraform init  
$ terraform plan  