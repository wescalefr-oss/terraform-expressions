locals {
    all_sg_rules = [
      {
        name = "ig_http"
        type = "ingress"
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        name = "ig_all"
        type = "ingress"
        from_port = 0
        to_port = 65535
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        name = "ig_https"
        type = "ingress"
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        name = "ig_ssh"
        type = "ingress"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        name = "eg_all"
        type = "egress"
        from_port = 0
        to_port = 65535
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        name = "eg_google_dns"
        type = "egress"
        from_port = 53
        to_port = 53
        protocol = "tcp"
        cidr_blocks = ["8.8.8.8/32"]
      },
      {
        name = "ig_restricted"
        type = "ingress"
        from_port = 0
        to_port = 65535
        protocol = "tcp"
        cidr_blocks = ["1.1.1.1/32"]
      }
    ]

    rulenames_by_sgs = {
      "http" = [
        "ig_http",
        "ig_https",
        "eg_all"
      ]
      "full_in" = [
        "ig_all",
        "eg_google_dns"
      ]
      "dns" = [
        "ig_restricted",
        "eg_google_dns"
      ]
    }
}