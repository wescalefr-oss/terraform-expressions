locals {
    all_sg_rules = [
      {
        sg_name = "http"
        type = "ingress"
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        sg_name = "full_ingress"
        type = "ingress"
        from_port = 0
        to_port = 65535
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        sg_name = "http"
        type = "ingress"
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        sg_name = "ssh"
        type = "ingress"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        sg_name = "http"
        type = "egress"
        from_port = 0
        to_port = 65535
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        sg_name = "egress_google_dns"
        type = "egress"
        from_port = 53
        to_port = 53
        protocol = "tcp"
        cidr_blocks = ["8.8.8.8/32"]
      }
    ]
}