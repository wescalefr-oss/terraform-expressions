# terraform-expressions

Exercice 2 :

Le but de cet exercice est de créer les rules du security group à partir de la liste disponible en variable locale, mais uniquement celles correspondant au nom du security group (3 rules).


les commandes pour tester votre code terraform : 

$ export AWS_ACCESS_KEY_ID="anaccesskey"  
$ export AWS_SECRET_ACCESS_KEY="asecretkey"  
$ terraform init  
$ terraform plan  