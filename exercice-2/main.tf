data "aws_vpc" "default" {
  default = true
}

resource "aws_security_group" "http" {
  name        = "http"
  description = "Allow http(s) inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  tags = {
    Name = "http"
  }
}